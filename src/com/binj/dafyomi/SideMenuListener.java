package com.binj.dafyomi;

public interface SideMenuListener {
    public void onMenuItemClicked(int pos);
}
