package com.binj.dafyomi;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class HomeFragment extends Fragment {

    private SideMenuListener mListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (SideMenuListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                            + " must implement SideMenuListener");
        }
    }

    public static HomeFragment newInstance() {
        HomeFragment f = new HomeFragment();

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                    Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_home, container, false);

        return v;
    }
}
