package com.binj.dafyomi;

import android.os.Bundle;
import android.support.v4.app.Fragment;

public class MainActivity extends BaseActivity implements SideMenuListener {

    private Fragment homeFragment;

    public MainActivity() {
        super(R.string.app_name);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set the Above View
        if (savedInstanceState != null)
            homeFragment = getSupportFragmentManager().getFragment(
                            savedInstanceState, "mContent");
        if (homeFragment == null)
            homeFragment = HomeFragment.newInstance();

        // set the Above View
        setContentView(R.layout.content_frame);
        getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, homeFragment, "home")
                        .commit();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        getSupportFragmentManager().putFragment(outState, "mContent",
                        homeFragment);
    }

    public void switchContent(Fragment fragment, String title) {
        homeFragment = fragment;
        getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, fragment).commit();
        getSlidingMenu().showContent();

        setTitle(title);
    }

    @Override
    public void onMenuItemClicked(int pos) {
        Fragment newContent = null;
        switch (pos) {
        case 0:
            newContent = new ColorFragment(R.color.red);
            break;
        case 1:
            newContent = new ColorFragment(R.color.green);
            break;
        case 2:
            newContent = new ColorFragment(R.color.blue);
            break;
        case 3:
            newContent = new ColorFragment(android.R.color.white);
            break;
        case 4:
            newContent = new ColorFragment(android.R.color.black);
            break;
        }
        if (newContent != null)
            switchContent(newContent, "yay");
    }

}
